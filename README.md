## GENERAL
1. 如果 修改代码之后 发现存读档(或者 返回标题界面) 出错，先删除 SaveData 文件夹，再次进行测试；因为代码修改后原来的存档文件可能不通用。
2. To: 程序员  Story目录下的脚本的文件名解释： 
  * TBR: to be revised (修改完成之后会改成TBC)
  * TBC: to be coded


###目标 —— 制作最简短的游戏程序
    1. 主要剧情严选
    2. 具有现实感的角色心理描写
    3. 中国二三线城市市井生活的诚实再现

主控思想：

    1. 制作描写中国高中生活、受资本和阶级制约的应试教育制度、以及生活在其中的学生和老师的个体命运。
    2. 少年的个性与意气被学校压抑。因为无论是表教育界还是里教育界，都将高考而不是个人培养放在第一位，不择手段，同时又没有人能对他们的个性作出合适的引导与帮助。

###链接：
1. BKengine下载：https://bke.bakery.moe/download.html
2. 程序下载：https://bitbucket.org/superclass2/gkbh/get/aa6730a8f9f3.zip
3. ~~信息更新在维基上：wiki.superclass2.ml~~   
4. 文件共享在百度网盘上：blessingsoftware@163.com
  pw:见微信群公告
5. 代码更新在bitbucket上：https://bitbucket.org/superclass2/gkbh/src
6. git的使用方法（过时了）：http://wiki.superclass2.ml/images/7/70/Use_git_on_windows.pdf

7. 宣传用网站：https://class-two.com
8. 开发看板： https://trello.com/invite/b/WR8NDMlD/8105bdc35c405ca94f7b978bb649d49e/gkbh

###进度：
2019-04-05: first version release (v1.9)
2018-07-19: demo2.4
# RPG模式UI说明

＊注：前往天台、文学社、模联的按钮还没有实现，计划之后放在画面的左侧。暂时先保持现有的按钮不变。

![UI Club Festival 0](UI Club Festival 0.png)

图1: 整体是一个卷轴模式，黄色方框是800x600的实际显示区域。使用 “去上面”，“去下面” 按钮移动地图。最好能做出移动时的动画效果。



![UI Club Festival preview 1](UI Club Festival preview 1.png)

图2: （最上面的区域）鼠标悬停在社团区域上面时出现社团名和箭头，点击后开始对话。



![UI Club Festival preview 2](UI Club Festival preview 2.png)

图3: 对话时“去上面”和“去下面”的按钮消失，其他社团的对话气泡消失，此时鼠标点击只触发下一句对话，地图上的社团不接受点击。



![UI Club Festival preview 3](UI Club Festival preview 3.png)

图4: （中间区域） 注意此时只允许3个社团可以接受点击。溢出地图的社团——例如KissxQuiz——并不会显示黄色的对话气泡，同时鼠标悬停也不会有悬停效果。



![UI Club Festival preview 4](UI Club Festival preview 4.png)

图5: （中间区域）与斗兽棋社对话时的效果



![UI Club Festival preview 5](UI Club Festival preview 5.png)

图6: （下方区域）注意这时候只有“去上面”按钮。可以点击的社团只有KissxQuiz和理财社。
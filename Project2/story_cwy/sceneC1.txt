2012.2.27 （西西弗收到 严重警告 通知书）
【上接Scene 18】
【CWYBranchScene 1】
……
茨维娅：哼！肯定是这样的！你不是总念叨着要遵守校规吗？不是叫我们要认真学习吗？不是你这种人勾结夏甲和学生会，还能是谁？
真穗：我……我……
茨维娅：还要狡辩吗！
男主：茨维娅！不要污蔑人！
茨维娅：你……我还以为你当时敢于出手打架，是站在我这边的呢……
男主：你不要指责真穗了！她绝对没有告密！
真穗：我……我……
茨维娅：好吧！你们不走，我走！
茨维娅愤怒地跑远了。

怎么办？
我们内部的关系居然会变成这样……
有什么办法吗？
【茨维娅】
【以下走廊-楼梯各种变换】
来不及想那么多了。[l] 
我本能地拔脚也冲进了楼道深处。[l] 
背后，奈鹤和涛涛还呆呆站着，但我已经管不了那么多了。[l] 
茨维娅去哪里了？
是往楼上跑了还是楼下？
→楼上
→楼下
【楼上选择支】
楼上是通向办公楼的天桥。[l] 
我奔跑着，在办公楼里左右张望。[l] 
并没有看到真穗的身影。[l] 
或许，在楼下？
接【楼下选择支】
【楼下选择支】
楼下通往车库和地面。[l] 
还有一排排阴森的实验室。[l] 
听，我听到了断续的哭声。[l] 
是从那边传来的。[l] 
我在走廊里奔跑，那边是教学楼西面的出口。[l] 
@seScaled channel=0 file="resource/se/running.ogg"
茨维娅的身影站在那里，手锤着墙壁。[l] 
不愿意跑出楼去，是害怕被人看到吧。[l] 
我气喘吁吁，跑到了她身边。[l] 
【茨维娅泪颜】
【悲伤交谈的音乐】
茨维娅：你……
西西弗：不好意思……真的……但……我们真的不是那个意思……
茨维娅：你……
西西弗：茨维娅，真穗真的不是那种人，是你太多疑了……



“啪！”
@seScaled channel=0 file="resource/se/slap.ogg"
我的脸上挨了一记耳光。我呆呆站着。
茨维娅：够了！还没够吗！你们，他们，你，都在指责我！都是我的错吗！我受不了了！
茨维娅转过身去，头靠在墙上，止不住地哭泣。
空气仿佛凝固了。周围的一切也变得安静，只有茨维娅的抽泣声。我的大脑一片空白。
茨维娅：对……对不起……
我听到细若游丝的声音。
西西弗：嗯嗯？？
茨维娅：实在……对不起……不应该……把失败……怪到你们身上……
西西弗：……嘛……我们……也有不对的地方……
我实在不知道该说什么好，才能安慰眼前的女孩。
茨维娅：都是……都是我……都是我的错……都是我不对……害了大家……
茨维娅又一次忍不住，顺着墙瘫坐在地上，开始哭泣。
我也靠着墙坐在她旁边，手轻轻扶着她的肩头。
西西弗：……我……我也不知道该说什么安慰的话呢……
西西弗：但是……你想听我坦率的想法吗？
茨维娅：……嗯？
西西弗：也许……校园革命……是无论怎样都无法成功的呢……但是……这件事能发生本身，不是很厉害吗……
茨维娅：……
西西弗：能够……挑战那么强大的大人，能够把大家的激情都点燃，能够给全体的学生带来希望，已经是不可思议的事情了呢……即使最后失败，也没什么好后悔的吧。
茨维娅：……是……是这样吗……
西西弗：毕竟，已经那样青春地展现过一回了呢。
一股强烈的感情涌上我的喉头。
西西弗：而且……看到那样帅气，那样厉害的茨维娅……我很憧憬呢。
茨维娅：……诶？
西西弗：我……很喜欢那样的茨维娅呢……
茨维娅：……真的……喜欢？
西西弗：……嗯……

这是？！！！
我还没有反应过来，茨维娅贴到了我的脸上。
这是……KISS？！！！！！！！！！！
我脑海一片空白，茨维娅还留着泪痕的脸现在正笑着看着我。

西西弗：这……这还是……第一次……KISS啊……
茨维娅嘟起嘴。
茨维娅：不知道哦~完全无所谓~
茨维娅：呐，我们逃跑吧！
西西弗：去，去哪里？
茨维娅：不知道哦~无所谓的吧~跟我来就好了！
说完，茨维娅抓起我的手，就往学校后院跑去。
我一路跟着跑，操场上的零星学生，都瞪大了眼睛看着我们。
西西弗：喂……很难为情耶！
这一段的栏杆比较矮，翻出去就是咖啡馆那里。
茨维娅几下就翻过了学校栏杆，到了栏杆另一边。
茨维娅：快，你也翻过来吧！
西西弗：嗯……
我蹬蹬几脚，翻过了栏杆。刚落地，又被茨维娅拉着跑去汽车站。
茨维娅：看啦！168路来了！
我不知所措地上了车。
茨维娅掏出随身钱包刷了两次公交卡。
西西弗：所以……咱们这是去哪？
茨维娅：去火车站呀！然后坐高速列车逃跑！
西西弗：喂声音小点……周围人都看着……高速列车又去哪？
茨维娅：啊哈哈，无所谓啦！去遥远的地方就好了！去远方流浪！
公共汽车上的人都用怪异的表情看着大声说话的茨维娅……和我。
西西弗：那……总得有个方向吧！
茨维娅：去西边！沿江一直向西，去边境的森林山脉，然后加入当地反抗游击队吧！
西西弗：小……小声点……这也……太……
茨维娅：或者去东边！一直去入海口，去神滨自由市，然后从那里坐轮船去国外！
西西弗：……
我看着茨维娅的眼睛。她的眼神与其说是坚定，不如说有一丝疯狂的色彩。一种我难以理解，感到……恐惧，却又被吸引的眼神。
茨维娅：怎么？害怕了？你不是喜欢我吗？
公交车上的乘客已经当我们是不良少年……不……已经是犯罪后的亡命情侣了吧……
西西弗：嗯……
茨维娅：那就跟我逃跑吧！
西西弗：我……其实在这里还有……组织盯着我……
乘客会报警的吧？！
【三六南路站，到了】
茨维娅：什么……
西西弗：我……现在还没法走……其实……我的真实身份是……
茨维娅：什么呀，果然还是个怂逼吗？
西西弗：那个……我……不行……就是……
茨维娅：那就滚下去吧！
茨维娅朝我怒吼。
我没有握紧扶手，一下就被茨维娅推下了车，摔在地上。
车门关上了，我最后一眼，看到茨维娅鄙夷的眼神。

【背景：公交车站】
这一切都是怎么回事……我瘫坐在地上。
“找死吗！”一声怒喝震破我的耳膜。
回头一看，一个中年人骑着自行车，在我面前急刹车。
“要死吗！年纪轻轻，躺在路上碰瓷吗！”那个人朝我大吼。
我自知理亏，急忙起身跑到人行道上。
那人嘴里还在骂骂咧咧，但是好歹骑车走掉了。身边的市民盯着我的目光也都移开了。
我拍了拍身上的灰，靠着墙仔细回忆今天的事情。
没人来打搅我，不管闲事的社会果然还是有好处……
所以是在KISS了一刻钟后被甩了吧！
我为什么不敢逃跑呢？我回忆着当时混乱的心理状态。
大概是……很多念头当时一拥而上，只有模糊的声音告诉自己不能走……
因为是高考猎人，如果贸然跑了会被组织追杀吧！
因为听到茨维娅说的，很害怕，那什么……游击队？都是什么奇怪的……
因为身上什么都没有？书包，钱包，所有东西都在这里，所以条件发射觉得不能走？
总之这一切都发生得太快，太混乱了……
我的感情是真的吗？我……确实喜欢茨维娅。
我朝车开去的方向跑了几步，又停下来了。
车已经开走好几分钟了，现在追不上了吧。或者我现在赶去火车站？
身上确实一分钱都没有。现在唯一的办法……只有先回学校取……
糟糕，这一块的城区，我不熟悉啊！
……
【道路】
【校门】
向街边的杂货店主打听了路线，我一路小跑了半小时回到学校，又翻过围墙进来了。
【走廊】
【教室】
现在第一节课正在进行……我从后门冲进了教室，抱起书包就跑。
老师：西西弗！你干什么呢？！
同学用惊愕的眼神看着我。
管不了那么多了……我又一路跑回去，翻过围墙，然后拦住一辆亮着载客的出租车。
西西弗：司机师傅！麻烦去火车站！
司机：不顺路！不去！
出租车扬长而去，原来车上其实有乘客啊。
我不停地挥手，终于有辆真正的空车停了下来。
西西弗：师傅！去火车站！麻烦最快速度！
【汽车】
找到她应该怎么说呢？首先应该告诉她我的身份……然后一起讨论该怎样离开这里吧……只要准备一下，我就和她一起走好了！
【火车站】
人头攒动的火车站，要找到一个人就像大海捞针啊……
我在候车大厅里来回找，也没有看到茨维娅的身影。
难道……她已经上车了？
我掉回头看着巨大的火车时刻显示屏。
会往哪去啊……往东……还是往西……往西可能去……去国界线是吗……宁水，九宫岭，二间川……
往东的火车……去天港？神滨？不知道啊……在我耽误的这一小时里已经有车开走了……
怎么办？！有了……
我拿出手机，回拨了涛涛的电话。

从涛涛那里拿到了茨维娅的号码。
我不停地打着茨维娅的电话，但是一直被挂断，到后面变成已关机了。
茨维娅果然很生气吗。虽然她很厉害，但一个人去很远的地方，即使除掉喜欢她因此挂念她的因素，果然还是很担心啊。

之后的我回到学校，被老师骂了一顿说处分在身又擅自旷课。

【过天】
2017.02.28
真穗&奈鹤：西西弗,我有事问你！
西西弗：什么事情……
真穗：有人……看到你和茨维娅一起出学校了……违反校规翻墙出去了……为什么茨维娅不来上课了？
这该怎么回答呢……
西西弗：其实……我也不太知道……可能……因为很伤心吧。
奈鹤：所以……那天后来你们之间到底发生了什么？
西西弗：呃……这个……她自己坐公交车走了。
真穗：那很奇怪啊……是回家了吗……你知道她家里电话吗……
西西弗：我……不知道……
奈鹤：太奇怪了……刚才我问了老师……老师说他也不知道……
涛涛：我就此事询问了教导处，对方回答说不必担心，但拒绝透露更多信息。
奈鹤：什么啊！这是什么意思！故意不告诉我们同学情况吗？
西西弗：那……不用担心是吗……
真穗：不用担心么？太好了……但是还是想知道到底发生什么事情……后来你又来班上一趟是怎么回事？
西西弗：我……好吧，我当时回教室想……拿钱包，然后坐车去追茨维娅，但是完全追不上啊……
奈鹤：那你知道她后来去哪里了吗？
西西弗：我也想知道……但是……真的不知道啊。
真穗：啊啊……那……很奇怪呢……
大家带着疑惑走开了。
【】之后的每天，我都尝试给茨维娅打电话，希望能和她说上话，但是对方一直处于关机状态。她到底去了哪里呢……会去做什么呢……
【】有时我想象她加入了边境的叛军游击队，有时候又想象她在国际轮船上。这些都……不太可能吧。但是学校说不必担心……那又是……什么意思呢？
